Feature: Pick Delivery
  As a courier I want to pick a delivery so that I can earn money.


  Background:

    Given cities:
      | postal| name                      | country       |
      | 2000  | Antwerpen                 | BELGIUM       |
      | 2600  | Berchem                   | BELGIUM       |
      | 2040  | BerendrechtZandvlietLillo | BELGIUM       |
      | 2140  | Borgerhout                | BELGIUM       |
      | 2100  | Deurne                    | BELGIUM       |
      | 2180  | Ekeren                    | BELGIUM       |
      | 2660  | Hoboken                   | BELGIUM       |
      | 2170  | Merksem                   | BELGIUM       |
      | 2610  | Wilrijk                   | BELGIUM       |
      | 90210 | Beverly Hills             | UNITED_STATES |

    #Your local eatery is at a distance close enough for pickup
    #Remote restaurant is at a distance too far away for pickup
    Given restaurants:
      | Name                                                       | adress_lat| adress_long|city|
      | Your local eatery                                          | 51.210150 | 4.397607   |0   |
      | Remote restaurant                                          | 60        | 10         |9|

    Given customers:
      | mail                   | mobile    | firstName | lastName  |
      | levifranzen@gmail.com  | 048562675 | Levi      | Franzen   |

      #city_id is the n-th number in de cities collection
      #customer_id is the n-th number in the customers collection. The adress is added to the delivery addresses.Scenario:
      #isPrimary: The adress is the primary contact adress of the user.
    Given deliveryadresses:
      | street          | number | city_id | lattitude | longitude | customer_id | isPrimary |
      | Nationalestraat | 10     | 0       | 51.214236 | 4.398242  | 0           | 1         |
      | Gravinstraat    | 19     | 3       | 51.215090 | 4.443523  | 0           | 0         |

  #By default couriers are inactive
    Given Couriers:
      | FirstName | LastName       | Street      | Number | city_id | adress_lat   |adress_long | mail       | tel       | current_lattitude | current_longitude| bankAccount         |
      | Sue       | Around         | Volksstraat | 10     | 0       | 51.211759    | 4.396674   | sue@kdg.be | 032545856 | 51.219090         | 4.399394         | BE11111111111111111 |
      | Karl      | Offenbach      | Pijkedreef  | 18     | 0       | 51.215159    | 4.396622   | karl@kdg.be| 033441856 | 51.219090         | 4.399394         | BE22222222222222222 |

      #resto_id is the n-th number in de cities collectionGiven Dishes
    Given Dishes:
      |id| name              | description                               | price | preperationTime | maxDeliveryTime | resto_id |
      |0| Ravioli Summervibes| Ravioli with rucola & Parmezaan           | 17.5  | 30              | 20              | 0        |
      |1| Spaghetti frivol   | Grannies recipe                           | 20    | 15              | 25              | 1        |

    
#  DONE
  Scenario: A "placed order" invisible 4th minute when deliverypoints Courier below 200.
    Given an order with description "4thminute" for dish with id 0 happened 4 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 100 deliveryPoints
    And Courier 1 is active and has 300 deliveryPoints
    When Courier 0 asks for list of available deliveries
    Then Courier gets an empty list

# DONE
  Scenario: A "placed order" visible 4th minute when deliverypoints Courier above 200.
    Given an order with description "4thminute" for dish with id 0 happened 4 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 300 deliveryPoints
    And Courier 1 is active and has 100 deliveryPoints
    When Courier 0 asks for list of available deliveries
    Then Courier gets a deliverylist with 1 order

# DONE
  Scenario: A "already accepted order" not visible 4th minute when deliverypoints Courier above 200.
    Given an order with description "4thminute" for dish with id 0 happened 4 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 300 deliveryPoints
    And Courier 1 is active and has 100 deliveryPoints
    And Courier 1 selects available order 0 to deliver
    When Courier 0 asks for list of available deliveries
    Then Courier gets an empty list

# DONE
  Scenario: A "placed order" visible 6th minute when deliverypoints Courier below 200.
    Given an order with description "6thminute" for dish with id 0 happened 6 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 100 deliveryPoints
    And Courier 1 is active and has 300 deliveryPoints
    When Courier 0 asks for list of available deliveries
    Then Courier gets a deliverylist with 1 order
    And delivery 0 of the deliverylist has description "6thminute"

# DONE
  Scenario: A "placed order" too far a away 6th minute when deliverypoints Courier below 200.
    Given an order with description "6thminute" for dish with id 0 happened 6 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 100 deliveryPoints
    # about 12 km away, at 15 km/h the courier 0 will not be able to get tot the resto within the preparation time of 30'
    And Courier 0 has position 51.1842807 4.5824398
    And Courier 1 is active and has 300 deliveryPoints
    When Courier 0 asks for list of available deliveries
    Then Courier gets an empty list

# DONE
# note: this is the only real scenario from the pick deliveries use case
  # the above scenario's are more related to the show available orders scenario
  Scenario: Courier selects order and receives deliveryPoints.
    Given an order with description "6thminute" for dish with id 1 happened 6 minutes in the past and has state "ORDER_PLACED" placed by customer 0
    And Courier 0 is active and has 100 deliveryPoints
    When Courier 0 selects available order 0 to deliver
    Then state of order 0 is 'ACCEPTED_BY_COURIER'
    And Order 0 has courier 0 assigned
    And Courier 0 has an deliveryPoint record with type 'MISSION_ACCEPTED' with 50 points