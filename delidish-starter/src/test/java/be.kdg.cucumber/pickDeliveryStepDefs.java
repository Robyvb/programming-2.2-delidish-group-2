package be.kdg.cucumber;


import be.kdg.delidish.domain.common.Address;
import be.kdg.delidish.domain.common.City;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.common.Position;
import be.kdg.delidish.domain.order.*;
import be.kdg.delidish.domain.payment.Money;
import be.kdg.delidish.domain.person.Courier;
import be.kdg.delidish.domain.person.Customer;
import be.kdg.delidish.domain.person.DeliveryPointEvent;
import be.kdg.delidish.domain.person.Partner;
import be.kdg.delidish.domain.restaurant.Dish;
import be.kdg.delidish.domain.restaurant.Restaurant;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDateTime;
import java.util.*;
import be.kdg.delidish.domain.system.DeliDishSystem;


public class pickDeliveryStepDefs {
    DeliDishSystem r;

    @Before
    public void init() {
        // initialise your system
        System.out.println("TODO: initialising Delidish system for tests");
        r = new DeliDishSystem();
    }

    // @Given: read local data and store them in collections of domain objects that are attributes of this class
    @Given("restaurants:")
    public void restaurants(DataTable dataTable) {
        System.out.println("TODO: Initialising restaurants");
        for (Map<String, String> map : dataTable.asMaps()) {
            Position pos=new Position(Double.parseDouble(map.get("adress_long")),Double.parseDouble(map.get("adress_lat")));
            Address address = new Address();
            address.setPosition(pos);
            address.setCity(Integer.parseInt(map.get("city")));
            ContactInfo continfo=new ContactInfo();
            continfo.setAddress(address);
            r.addRestaurant(new Restaurant(map.get("Name"),continfo));
        }

    }

    @Given("cities:")
    public void cities(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            r.addAddress(new Address(
                    new City(map.get("postal"), map.get("name")),
                    map.get("country")));
        }
    }

    @Given("customers:")
    public void customers(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            r.addCustomer(new Customer(new Address(), map.get("mail"),
                    map.get("mobile"), map.get("firstName"), map.get("lastName")));
        }
    }

    @Given("deliveryadresses:")
    public void deliveryAdresses(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            Address address= new Address(Integer.parseInt(map.get("city_id")), map.get("street"), map.get("number"),
                    new Position(Float.parseFloat(map.get("longitude")),
                            Float.parseFloat(map.get("lattitude"))), "Belgium");
            address.setPrimary(Integer.parseInt(map.get("isPrimary")) == 1);
            r.getCustomers().get(Integer.parseInt(map.get("customer_id"))).addAddress(address);
            r.addresses.add(address);
        }
    }

    @Given("Couriers:")
    public void couriers(DataTable dataTable) {
        for (Map<String, String> map : dataTable.asMaps()) {
            Position pos=new Position(Double.parseDouble(map.get("adress_long")),Double.parseDouble(map.get("adress_lat")));
            Address address = new Address(Integer.parseInt(map.get("city_id")),map.get("Street"),
                    map.get("Number"),pos);
            ContactInfo continfo=new ContactInfo(address,map.get("email"),map.get("tel"));
            Courier c=new Courier(continfo,map.get("FirstName"),map.get("LastName"), true,
                    new Position(Float.parseFloat(map.get("current_longitude")), Float.parseFloat(map.get("current_lattitude"))),
                    new Partner(map.get("bankAccount")));
            r.addCourier(c);
        }
    }

    @Given("Dishes:")
    public void dishes(DataTable dataTable) {
        //TODO: add dishes and link them to their restaurant
        for (Map<String, String> map : dataTable.asMaps()) {
            Restaurant res=r.getRestaurants().get(Integer.parseInt(map.get("resto_id")));
            Dish dish=new Dish(map.get("name"),map.get("description"), new Money(Money.Currency.EURO, Float.parseFloat(map.get("price"))), Integer.parseInt(map.get("preperationTime")),
                    Integer.parseInt(map.get("maxDeliveryTime")));
            res.addDish(dish);
            r.addDish(dish);
        }
    }

    @Given("an order with description {string} for dish with id {int} happened {int} minutes in the past and has state {string} placed by customer {int}")
    public void anOrderWithDescriptionForDishWithIdHappenedMinutesInThePastAndHasStatePlacedByCustomer(
            String discription,
            int dishId,
            int minutesAgo,
            String state,
            int customer) {
        Order newOrder=new Order(dishId, List.of(new OrderEvent(LocalDateTime.now().minusMinutes(minutesAgo), OrderState.ORDER_PLACED, discription)));
        r.addOrder(newOrder);
        r.getCustomers().get(customer).addOrder(newOrder);
        newOrder.getOrderLines().add(new OrderLine(1, state));
        Dish newDish = r.restaurants.get(0).getMenu().get(0);
        newOrder.getOrderLines().get(0).setDish(newDish);
        newOrder.setDeliveryAddress(r.getCustomers().get(customer).getDeliveryAddresses().get(0));
        System.out.println("TODO: write GIVEN");
    }

    @And("Courier {int} is active and has {int} deliveryPoints")
    public void courierIsActiveAndHasDeliveryPoints(int courierIndex, int delPoints) {
        System.out.println("TODO: write AND GIVEN");
        DeliveryPointEvent dpe=new DeliveryPointEvent(LocalDateTime.now(), delPoints, EventType.MISSION_ACCEPTED);
        r.getCouriers().get(courierIndex).addDeliveryPointEvent(dpe);

    }

    @When("Courier {int} asks for list of available deliveries")
    public void courierAsksForListOfAvailableDeliveries(int arg0) {
        System.out.println("TODO: write WHEN");
         r.showAvailableMissions(r.getCouriers().get(arg0).getCurrentPosition(), r.getCouriers().get(arg0).getPoints());
    }

    @Then("Courier gets an empty list")
    public void courierGetsAnEmptyList() {
        System.out.println("TODO: write THEN Asserts");
        assertEquals(0, r.showAvailableMissions(r.getCouriers().get(0).getCurrentPosition(), r.getCouriers().get(0).getPoints()).size());
    }

    @Then("Courier gets a deliverylist with {int} order")
    public void courierGetsADeliverylistWithOrder(int count) {
        System.out.println("TODO: write THEN Asserts");
        assertEquals(count, r.showAvailableMissions(r.getCouriers().get(0).getCurrentPosition(), r.getCouriers().get(0).getPoints()).size());
    }


    @When("Courier {int} selects available order {int} to deliver")
    public void courierSelectedAvailableOrderToDeliver(int courierIndex, int orderId) {
        System.out.println("TODO: write WHEN");
        r.pickDelivery(courierIndex,orderId);
    }

    @And("Courier {int} has position {float} {float}")
    public void courierHasPosition(int courierIndex, float longitude, float latitude) {
        System.out.println("TODO: write AND ");
        r.getCouriers().get(courierIndex).setCurrentPosition(new Position(longitude, latitude));
    }


    @Then("state of order {int} is {string}")
    public void stateOfOrderIsCOURIER_ASSIGNED(int orderId, String state) {
        System.out.println("TODO: write THEN Asserts");
        assertTrue(r.getOrders().get(orderId).getOrderEvents().get(0).getState().toString().equalsIgnoreCase(state));
    }

    @And("Order {int} has courier {int} assigned")
    public void orderHasCourierAssigned(int orderId, int courierIndex) {
        System.out.println("TODO: write THEN Asserts");
        Order order=r.getOrders().get(orderId);
        Courier courier=r.getCouriers().get(courierIndex);
        assertSame(order.getCourier(),courier);
    }

    @And("Courier {int} has an deliveryPoint record with type {string} with {int} points")
    public void courierHasAnDeliveryPointRecordWithTypeWithPoints(int courierIndex,
                                                                  String type,
                                                                  int points) {
        System.out.println("TODO: write THEN Asserts");
        Courier courier = r.getCouriers().get(courierIndex);
        assertEquals(type, courier.getDeliveryEvents().get(0).getEventType().toString());
    }


    @And("delivery {int} of the deliverylist has description {string}")
    public void deliveryOfTheDeliverylistHasDescription(int arg0, String arg1) {
        System.out.println("TODO: write AND");
        assertEquals(arg1, r.orders.get(arg0).getOrderEvents().get(arg0).getRemark());
    }
}

