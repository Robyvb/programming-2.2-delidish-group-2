package be.kdg.delidish.domain.restaurant;

public enum Allergen {
    GLUTEN, DAIRY, EGGS, NUTS
}
