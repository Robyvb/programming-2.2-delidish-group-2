package be.kdg.delidish.domain.person;

import java.util.*;
import be.kdg.delidish.domain.common.Address;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.order.Order;

public class Customer extends Person {

    private List<Address> deliveryAddresses;
    private List<Order> orders;


    public Customer() {
        super();
        deliveryAddresses= new ArrayList<>();
        orders= new ArrayList<>();
    }

    public Customer(Address address, String email, String phone, String firstName, String lastName) {
        super(new ContactInfo(address, email, phone), firstName,  lastName);
        deliveryAddresses= new ArrayList<>();
        orders= new ArrayList<>();
    }

    public Order addOrder(Order newOrder){
        orders.add(newOrder);
        return newOrder;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<Address> getDeliveryAddresses() {
        return deliveryAddresses;
    }

    public void addAddress(Address address){
        deliveryAddresses.add(address);
    }

}
