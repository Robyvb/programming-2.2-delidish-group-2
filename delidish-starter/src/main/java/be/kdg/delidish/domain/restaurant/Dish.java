package be.kdg.delidish.domain.restaurant;

import be.kdg.delidish.domain.payment.Money;

import java.util.List;

public class Dish {
    private String name;
    private String description;
    private Money price;
    private int productionTime;
    private int maximumDeliveryTime;
    private List<Allergen> allergies;
    private boolean orderable;
    private Dish dishPart;
    private Restaurant restaurant;

    public Dish(String name, String description, Money price, int productionTime
    , int maximumDeliveryTime, List<Allergen> allergies, boolean orderable) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.productionTime = productionTime;
        this.maximumDeliveryTime = maximumDeliveryTime;
        this.allergies = allergies;
        this.orderable = orderable;
        dishPart=null;
    }

    public Dish(String name, String description, Money price, int productionTime, int maximumDeliveryTime) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.productionTime = productionTime;
        this.maximumDeliveryTime = maximumDeliveryTime;
        dishPart=null;
    }


    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void setDishPart(Dish dishPart){
        this.dishPart=dishPart;
    }

    public Dish getDishPart(){
        return dishPart;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Money getPrice() {
        return price;
    }

    public int getProductionTime() {
        return productionTime;
    }

    public int getMaximumDeliveryTime() {
        return maximumDeliveryTime;
    }

    public List<Allergen> getAllergies() {
        return allergies;
    }

    public boolean getOrderable() {
        return orderable;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = name;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public void setProductionTime(int productionTime) {
        this.productionTime = productionTime;
    }

    public void setMaximumDeliveryTime(int maximumDeliveryTime) {
        this.maximumDeliveryTime = maximumDeliveryTime;
    }

    public void setAllergies(List<Allergen> allergies) {
        this.allergies = allergies;
    }

    public void setOrderable(boolean orderable) {
        this.orderable = orderable;
    }
}
