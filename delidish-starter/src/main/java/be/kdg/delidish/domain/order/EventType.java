package be.kdg.delidish.domain.order;

public enum EventType {
    MISSION_ACCEPTED, TIMELY_PICKUP, LATE_PICKUP, TIMELY_DELIVERY, LATE_DELIVERY, DAILY_REDUCTION

//    private final String text;

    // private enum constructor
//    private EventType(String text) {
//        this.text = text;
//    }

}
