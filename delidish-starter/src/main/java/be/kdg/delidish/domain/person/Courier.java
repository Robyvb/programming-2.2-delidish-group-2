package be.kdg.delidish.domain.person;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.common.Position;
import be.kdg.delidish.domain.order.EventType;

import java.util.*;

public class Courier extends Person {

    private boolean isAvailable;
    private Position currentPosition;
    private List<DeliveryPointEvent> deliveryEvents;
    private Partner restOwner;

    public Courier() {
        this.deliveryEvents = new ArrayList<>();
    }

    public Courier(ContactInfo contactinfo, String firstName, String lastName){
        super(contactinfo,firstName,lastName);
        this.deliveryEvents= new ArrayList<>();
    }

    public Courier(ContactInfo contactinfo, String firstName, String lastName, boolean isAvailable, Position currentPosition, Partner restOwner) {
        super(contactinfo, firstName, lastName);
        this.isAvailable = isAvailable;
        this.currentPosition = currentPosition;
        this.restOwner = restOwner;
        this.deliveryEvents = new ArrayList<>();
    }

    public int getPoints(){
        int points=0;
        for(DeliveryPointEvent event: deliveryEvents){
            points+=event.getPoints();
        }
        return points;
    }

    public List<DeliveryPointEvent> getDeliveryEventByTypeAndPoints(int points, EventType type){
        List<DeliveryPointEvent> filteredEvents= new ArrayList<>();
        for (DeliveryPointEvent event: deliveryEvents){
            if (event.getPoints()==points && event.getEventType()==type){
                filteredEvents.add(event);
            }
        }
        return filteredEvents;
    }

    public Partner getRestOwner() {
        return restOwner;
    }

    public void setRestOwner(Partner restOwner) {
        this.restOwner = restOwner;
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Position currentPosition) {
        this.currentPosition = currentPosition;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailability(boolean availability){
        isAvailable=availability;
    }

    public List<DeliveryPointEvent> getDeliveryEvents(){
        return deliveryEvents;
    }

    public void addDeliveryPointEvent(DeliveryPointEvent event){
        deliveryEvents.add(event);
    }

    @Override
    public String toString() {
        return "Courier{" +
                "isAvailable=" + isAvailable +
                ", currentPosition=" + currentPosition +
                ", deliveryEvents=" + deliveryEvents +
                ", restOwner=" + restOwner +
                '}';
    }
}
