package be.kdg.delidish.domain.common;

public class Address {
    private int cityId;
    private String street;
    private String number;
    private Position position;
    private String country;
    private boolean isPrimary;

    public Address() {

    }

    public Address(City city, String country) {

    }

    public Address(int cityId, String country) {
        this.cityId = cityId;
        this.country = country;
    }
    public Address(int cityId, String street, String number, be.kdg.delidish.domain.common.Position position){
        this.cityId = cityId;
        this.street = street;
        this.number = number;
        this.position = position;
    }
    public Address(int cityId, String street, String number, be.kdg.delidish.domain.common.Position position, String country) {
        this.cityId = cityId;
        this.street = street;
        this.number = number;
        this.position = position;
        this.country = country;
    }


    public int getCity() {
        return cityId;
    }

    public void setCity(int cityId) {
        this.cityId = cityId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public be.kdg.delidish.domain.common.Position getPosition() {
        return position;
    }

    public void setPosition(be.kdg.delidish.domain.common.Position position) {
        this.position = position;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }
}
