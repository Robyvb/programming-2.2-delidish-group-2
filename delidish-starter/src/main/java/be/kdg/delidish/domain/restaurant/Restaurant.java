package be.kdg.delidish.domain.restaurant;
import be.kdg.delidish.domain.person.Partner;
import be.kdg.delidish.domain.common.ContactInfo;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    private static int restaurantId =0;
    private String name;
    private ContactInfo contactInfo;
    private List<OpeningPeriod> openingHours;
    private Partner owner;
    private List<Dish> menu;

    public Restaurant(String name, ContactInfo contactInfo) {
        restaurantId++;
        this.name = name;
        this.contactInfo = contactInfo;
        this.menu = new ArrayList<>();
    }

    public Restaurant(String name, ContactInfo contactInfo, List<OpeningPeriod> openingHours) {
        restaurantId++;
        this.name = name;
        this.contactInfo = contactInfo;
        this.openingHours = openingHours;
        menu=new ArrayList<>();
    }

    public void addDish(Dish dish){
        dish.setRestaurant(this);
        menu.add(dish);
    }

    public int getRestaurant_id() {
        return restaurantId;
    }

    public List<Dish> getMenu(){
        return menu;
    }

    public void setMenu(List<Dish> menu) {
        this.menu = menu;
    }

    public void setOwner(Partner owner) {
        this.owner = owner;
    }

    public Partner getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public List<OpeningPeriod> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<OpeningPeriod> openingHours) {
        this.openingHours = openingHours;
    }

    @Override
    public String toString() {
        return String.format("");
    }
}
