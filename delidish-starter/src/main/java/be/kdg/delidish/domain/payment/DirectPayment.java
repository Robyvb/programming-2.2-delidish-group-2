package be.kdg.delidish.domain.payment;

public class DirectPayment extends Payment {

    private String id;
    private PaymentType paymentType;

    enum PaymentType {
        CREDIT, DEBIT
    }

    public DirectPayment(String id, PaymentType paymentType) {
        super();
        this.id = id;
        this.paymentType = paymentType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Id: %s\nPayment Method: %s\n", id, paymentType.toString());
    }
}
