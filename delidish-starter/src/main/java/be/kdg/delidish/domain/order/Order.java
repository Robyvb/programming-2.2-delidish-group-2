package be.kdg.delidish.domain.order;

import be.kdg.delidish.domain.common.Address;
import be.kdg.delidish.domain.common.Position;
import be.kdg.delidish.domain.payment.DirectPayment;
import be.kdg.delidish.domain.payment.TransferPayment;
import be.kdg.delidish.domain.person.Courier;
import be.kdg.delidish.domain.person.Customer;
import be.kdg.delidish.domain.utils.DistanceCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Order {
    private int orderId;
    private Address deliveryAddress;
    private String deliveryInstructions;
    private int averageDeliveryPoints;
    private List<OrderEvent> orderEvents;
    private List<OrderLine> orderLines;
    private DirectPayment customerPayment;
    private TransferPayment restaurantPayment;
    private Customer customer;
    private Courier courier;

    public boolean isReachable(Position courierPos){
        double resLat = orderLines.get(0).getDish().getRestaurant().getContactInfo().getAddress().getPosition().getLatitude();
        double resLong = orderLines.get(0).getDish().getRestaurant().getContactInfo().getAddress().getPosition().getLongitude();
        double dist = DistanceCalculator.distance(courierPos.getLatitude(),courierPos.getLongitude(), resLat,resLong, "K");
        return dist <= maxDist();
    }

    public Order(int orderId, List<OrderEvent> orderEvents) {
        this.orderId = orderId;
        this.orderEvents = orderEvents;
        this.orderLines= new ArrayList();
    }

    public double maxDist(){
        double factor= 60/(double)getOrderTime();
        return 15/factor;
    }

    public int getOrderTime()
    {
        // This methode returns the approximate time for preparing the whole order
        int time=0;
        for (OrderLine ol: orderLines){
            time+=ol.getDish().getMaximumDeliveryTime();
        }
        return time;
    }

    public List<OrderEvent> getAvailableOrders() {
        return this.orderEvents.stream().filter(oe -> oe.getState() == OrderState.ORDER_PLACED).collect(Collectors.toList());
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryInstructions() {
        return deliveryInstructions;
    }

    public void setDeliveryInstructions(String deliveryInstructions) {
        this.deliveryInstructions = deliveryInstructions;
    }

    public int getAverageDeliveryPoints() {
        return averageDeliveryPoints;
    }

    public void setAverageDeliveryPoints(int averageDeliveryPoints) {
        this.averageDeliveryPoints = averageDeliveryPoints;
    }

    public List<OrderEvent> getOrderEvents() {
        return orderEvents;
    }

    public void setOrderEvents(List<OrderEvent> orderEvents) {
        this.orderEvents = orderEvents;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public DirectPayment getCustomerPayment() {
        return customerPayment;
    }

    public void setCustomerPayment(DirectPayment customerPayment) {
        this.customerPayment = customerPayment;
    }

    public TransferPayment getRestaurantPayment() {
        return restaurantPayment;
    }

    public void setRestaurantPayment(TransferPayment restaurantPayment) {
        this.restaurantPayment = restaurantPayment;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Courier getCourier() {
        return courier;
    }

    public void setCourier(Courier courier) {
        this.courier = courier;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", deliveryAddress=" + deliveryAddress +
                ", deliveryInstructions='" + deliveryInstructions + '\'' +
                ", averageDeliveryPoints=" + averageDeliveryPoints +
                ", orderEvents=" + orderEvents +
                ", orderLines=" + orderLines +
                ", customerPayment=" + customerPayment +
                ", restaurantPayment=" + restaurantPayment +
                ", customer=" + customer +
                ", courier=" + courier +
                '}';
    }
}
