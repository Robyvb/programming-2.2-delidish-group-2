package be.kdg.delidish.domain.person;

import be.kdg.delidish.domain.payment.TransferPayment;
import java.util.List;

public class Partner extends Person{
    private String accountNr;
    private List<TransferPayment> transferPayments;

    public Partner(String bankAccount){
        super();
    }

    public String getAccountNr() {
        return accountNr;
    }

    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

}
