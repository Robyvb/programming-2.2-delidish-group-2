package be.kdg.delidish.domain.person;

import be.kdg.delidish.domain.order.EventType;

import java.time.LocalDateTime;

public class DeliveryPointEvent {

    private LocalDateTime time;
    private int points;
    private EventType eventType;

    public DeliveryPointEvent(LocalDateTime time, int points,EventType eventType){
        this.time=time;
        this.points=points;
        this.eventType=eventType;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getPoints(){
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
}
