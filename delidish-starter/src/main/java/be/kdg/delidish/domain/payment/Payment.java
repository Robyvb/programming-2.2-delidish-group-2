package be.kdg.delidish.domain.payment;

import java.time.LocalDateTime;

public class Payment {

    private LocalDateTime time;
    private String detail;
    private Money amount;

    public Payment() {
        this.time = LocalDateTime.now();
        this.amount = new Money();
        this.detail = null;
    }

    public Payment(LocalDateTime time, String detail, Money amount) {
        this.time = time;
        this.detail = detail;
        this.amount = amount;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return String.format("Date: %s\nAmount: %.2f\nDetail: %s\n", time.toString(), amount, detail);
    }
}
