package be.kdg.delidish.domain.order;

import  be.kdg.delidish.domain.restaurant.Dish;
public class OrderLine {

    private int quantity;
    private String remark;
    private Dish dish;

    public OrderLine() {

    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public OrderLine(int quantity, String remark) {
        this.quantity = quantity;
        this.remark = remark;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    @Override
    public String toString() {
        return String.format("Quantity: %d\nRemark: %s", quantity, remark);
    }
}
