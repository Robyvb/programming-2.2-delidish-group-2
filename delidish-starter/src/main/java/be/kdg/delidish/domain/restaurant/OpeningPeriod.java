package be.kdg.delidish.domain.restaurant;

import be.kdg.delidish.domain.common.DayOfWeek;
import be.kdg.delidish.domain.common.Hour;

public class OpeningPeriod {
    private DayOfWeek dayOfWeek;
    private Hour openingTime;
    private Hour closingTime;

    public OpeningPeriod(DayOfWeek dayOfWeek, Hour openingTime, Hour closingTime) {
        this.dayOfWeek = dayOfWeek;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public Hour getOpeningTime() {
        return openingTime;
    }

    public Hour getClosingTime() {
        return closingTime;
    }
}
