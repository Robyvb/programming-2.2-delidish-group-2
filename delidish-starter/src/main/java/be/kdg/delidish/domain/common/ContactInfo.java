package be.kdg.delidish.domain.common;

public class ContactInfo {
    private be.kdg.delidish.domain.common.Address address;
    private String email;
    private String tel;

    public ContactInfo() {}

    public ContactInfo(be.kdg.delidish.domain.common.Address address, String email, String tel) {
        this.address = address;
        this.email = email;
        this.tel = tel;
    }

    public be.kdg.delidish.domain.common.Address getAddress() {
        return address;
    }

    public void setAddress(be.kdg.delidish.domain.common.Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
