package be.kdg.delidish.domain.payment;

public class Money {

    private Currency currency;
    private float quantity;

    public enum Currency {
        EURO, DOLLAR
    }

    public Money() {
        this.currency = null;
        this.quantity = 0.0f;
    }

    public Money(Currency currency, float quantity) {
        this.currency = currency;
        this.quantity = quantity;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public float getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return String.format("Currency: %s\nQuantity: %.2f\n", currency.toString(), quantity);
    }
}
