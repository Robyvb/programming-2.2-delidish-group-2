package be.kdg.delidish.domain.person;
import be.kdg.delidish.domain.common.ContactInfo;
public class Person {
    private ContactInfo contactinfo;
    private String firstName;
    private String lastName;

    public Person(){

    }

    public Person(ContactInfo contactinfo, String firstName, String lastName) {
        this.contactinfo = contactinfo;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public ContactInfo getContactinfo() {
        return contactinfo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setContactinfo(ContactInfo contactinfo) {
        this.contactinfo = contactinfo;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
