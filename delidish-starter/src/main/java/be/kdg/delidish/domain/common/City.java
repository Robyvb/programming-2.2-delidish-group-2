package be.kdg.delidish.domain.common;

public class City {
    private String postalCode;
    private String name;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City(String postalCode, String name) {
        this.postalCode = postalCode;
        this.name = name;
    }
}
