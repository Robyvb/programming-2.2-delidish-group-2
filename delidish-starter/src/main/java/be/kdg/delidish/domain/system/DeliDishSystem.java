package be.kdg.delidish.domain.system;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import be.kdg.delidish.domain.common.Address;
import be.kdg.delidish.domain.common.City;
import be.kdg.delidish.domain.common.Position;
import be.kdg.delidish.domain.order.Order;
import be.kdg.delidish.domain.order.OrderState;
import be.kdg.delidish.domain.person.Courier;
import be.kdg.delidish.domain.person.Customer;
import be.kdg.delidish.domain.restaurant.Dish;
import be.kdg.delidish.domain.restaurant.Restaurant;

public class DeliDishSystem {
    public List<Restaurant> restaurants;
    public List<Courier> couriers;
    public List<Address> addresses;
    public List<Customer> customers;
    public List<City> cities;
    public List<Order> orders;
    public List<Dish> dishes;

    public DeliDishSystem() {
        restaurants = new ArrayList<>();
        couriers = new ArrayList<>();
        customers = new ArrayList<>();
        addresses = new ArrayList<>();
        cities = new ArrayList<>();
        orders = new ArrayList<>();
        dishes = new ArrayList<>();
    }

    public List<Order> showAvailableMissions(Position courierPos, int courierPoints) {
        List<Order> availableOrders = new ArrayList<>();
        for (Order o : orders) {
            System.out.println(o);
            if (o.getOrderEvents().get(0).getState() == OrderState.ORDER_PLACED) {
                if (o.isReachable(courierPos)) {
                    if (courierPoints > 200) {
                        availableOrders.add(o);
                    } else if (courierPoints < 200 && Duration.between(o.getOrderEvents().get(0).getTime(), LocalDateTime.now()).toMinutes() > 5) {
                        availableOrders.add(o);
                    }
                }
            }
        }
        return availableOrders;
    }

    public void pickDelivery(int courierIndex, int orderId){
        Order order=orders.get(orderId);
        Courier courier = couriers.get(courierIndex);
        if (order.getOrderEvents().get(0).getState() == OrderState.ORDER_PLACED) {
            order.setCourier(courier);
            order.getOrderEvents().get(0).setState(OrderState.ACCEPTED_BY_COURIER);
        }
    }
    public void addRestaurant(Restaurant newRestaurant) {
        restaurants.add(newRestaurant);
    }

    public void addCourier(Courier newCourier) {
        couriers.add(newCourier);
    }

    public void addAddress(Address newAddress) {
        addresses.add(newAddress);
    }

    public void addCustomer(Customer newCustomer) {
        customers.add(newCustomer);
    }

    public void addOrder(Order newOrder) {
        orders.add(newOrder);
    }

    public void addDish(Dish dish){
        dishes.add(dish);
    }

    public void addCity(City city){
        cities.add(city);
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public List<Courier> getCouriers() {
        return couriers;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public List<City> getCities() {
        return cities;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

}
