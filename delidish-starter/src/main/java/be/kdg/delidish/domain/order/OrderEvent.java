package be.kdg.delidish.domain.order;

import be.kdg.delidish.domain.person.DeliveryPointEvent;

import java.time.LocalDateTime;

public class OrderEvent {

    private LocalDateTime time;
    private OrderState state;
    private String remark;
    private DeliveryPointEvent deliveryPointsEvent;

    public OrderEvent() {
        this.time = LocalDateTime.now();
        this.state = null;
        this.remark = null;
    }

    public OrderEvent(LocalDateTime time, OrderState state, String remark) {
        this.time = time;
        this.state = state;
        this.remark = remark;
    }

    public DeliveryPointEvent getDeliveryPointsEvent() {
        return deliveryPointsEvent;
    }

    public void setDeliveryPointsEvent(DeliveryPointEvent deliveryPointsEvent) {
        this.deliveryPointsEvent = deliveryPointsEvent;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return String.format("Date: %s\nState: %s\nRemark %s\n", time.toString(), state.toString(), remark);
    }
}
