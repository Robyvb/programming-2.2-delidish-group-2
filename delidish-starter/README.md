# Delidish-starter

Starter for the delidisch project using Java 11 and Gradle 

Feature files are under src\test\resources\features

Complete Cucumber testcode under src\test\java
( If your testcode is under another package, specify 
it in the glue attribute of be.kdg.cucumber.RunCucumberTest)

Open the Gradle window using the right border icon
In this window click Tasks > Verification > Test to run your tests 
(both JUnit and Cucumber tests will be executed)

build.gradle configuration is written in Groovy
	